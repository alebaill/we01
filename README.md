# We01 final

https://md.picasoft.net/s/eRFkwBu9F

créative common (peut-être copié modifié)

Sujet numéro 1
============

Le numérique, grande importance, grand impacte
-------

L'Arcep (Autorité de régulation des communications électroniques, des postes et de la distribution de la presse) 

"Il ne s'agit pas de condamner le numérique en lui-même ni de brider ou restreindre a priori son utilisation : certains usages participent directement à la réduction des émissions de gaz à effets de serre"

Ici l'Arcep souligne le fait que le numérique est une invention importante et qu'il ne faut pas s'en défaire. Mais il reste cependant des choses à faire.
En effet, De même que l'écrit a permis le passage du temporel au spatial par projection de la parole, le support numérique apporte de nouvelles formes de représentation des informations, basées sur le calcul. Bachimont parle de l'émergence d'une **« raison computationnelle »** ; Ainsi le numérique permet de retranscrire des documents etc par exemple et permet de faciliter le stockage de ces derniers et pleins d'autre chose. Cependant cela n'est pas sans conséquence pour la planète même si cela permet par exemple d'économiser en masse sur le papier et donc de polluer moins par son transport ou par la déforestation.

Il faut améliorer son utilisation
------- 

"il ne s'agit pas non plus de considérer le numérique comme un secteur dispensé des efforts à accomplir pour respecter l'Accord de Paris et ses exigences nouvelles" Cette citation de L'Arcep montre bien que selon eux le numérique aussi doit trouver le moyen d'être écologique. Par exemple prenons **"le capitalisme de surveillance"**, qui réprésente le fait que  l’industrie numérique prospère grâce à un principe presque enfantin : extraire les données personnelles et vendre aux annonceurs des prédictions sur le comportement des utilisateurs. "Au quotidien, nos échanges numériques et nos comportements de consommateurs sont enregistrés, mesurés, calculés afin de construire des profils qui s'achètent et se vendent." (cours)

Déjà cela n'est pas forcément bon au niveau des lois de la propriété privée mais ensuite et surtout cela consomme énormément d'énergie et relache énormément de CO2 notament par le biais de stockage de toute ces informations sur des Datas centers (un centre de données, ou centre informatique est un lieu regroupant des équipements constituants du système d'information d'une ou plusieurs entreprise. Il peut être interne et/ou externe à l’entreprise, exploité ou non avec le soutien de prestataires.) Par exemple : Si l’on se concentre sur l’Hexagone, 10 % de l’électricité produite sont consommées uniquement par des Data centers. Leur consommation représente autant qu’une ville de 50 000 habitants. Et 40% de cette consommation électrique est utilisée uniquement pour les refroidir. Ces machines néfastes pour la planète sont pourtant essentielles à l’industrie puisqu’elles collectent et rassemblent les données. 

Solution possible
-----------------

Arcep fait référence à l'accord de Paris dans sa citation.
Lors de la COP21 à Paris, en décembre 2015, on est parvenue à un accord historique pour lutter contre le changement climatique et pour accélérer et intensifier les actions et les investissements nécessaires à un avenir durable à faible intensité de carbone. L'Accord de Paris s'appuie sur la Convention et - pour la première fois - rassemble toutes les nations autour d'une cause commune pour entreprendre des efforts ambitieux afin de combattre le changement climatique et de s'adapter à ses conséquences, avec un soutien accru pour aider les pays en développement à le faire.

Ainsi pour essayer d'aller dans ce sens on peut voir plusieurs moyen d'y parvenir Selon l'Arcep il a 3 grands thèmes :

Notament en "améliorant la capacité de pilotage de l'empreinte environnementale du numérique par les pouvoirs publics".
En effet le pouvoir public a un énorme rôle à jouer, en mettant en place des lois pour réguler l'empreinte carbone par exemple?

Ensuite "intégrer l'enjeu environnemental dans les actions de régulation de l'Arcep".

il y a aussi le fait de "renforcer les incitations des acteurs économiques, acteurs privés, publics et consommateurs". Par exemple les particuliers pourraient utilisés le numérique de manière a diminuer leur empreinte carbone notament en triant leur mails en supprimant ce quk doit être supprimer etc et cela vaut aussi pour les entreprises. (cf mémoire) 

Conclusion
-----------

Pour conclure il ne faut par interdire le numérique ni le supprimer il faut mieux le gérer. "il est important que le numérique prenne « part à la stratégie bas carbone, sans renoncer aux possibilités d'échanges et d'innovation »". Il faut que chacun que soit du particulier au gouvernement s'associe pour mieux parvenir à atteindre une utilisation verte de ce dernie. Voila la position de l'Arcep selon moi.

*Creative Commons* : (on peut donc copier et rediffuser etc)

Sources
---------

Le cours

et :

[1](https://www.nextinpact.com/article/45093/pour-numerique-soutenable-bonnes-intentions-ce-nest-pas-suffisant)

[2](http://www.politis.fr/articles/2020/04/la-lutte-contre-la-surveillance-est-un-anticapitalisme-41802/)

[3](https://www.grizzlead.com/lincroyable-impact-de-la-pollution-numerique-et-les-bonnes-pratiques-a-adopter-tres-vite/)

[4](https://unfccc.int/fr/process-and-meetings/l-accord-de-paris/qu-est-ce-que-l-accord-de-paris)

*Lebailly Alexandre*